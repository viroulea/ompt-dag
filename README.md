This tool aims at creating a DAG for an OpenMP application (requires an OpenMP runtime with OMPT support).

Compile: `make` (requires a C++11 compiler)

Usage example: `OMP_TOOL_LIBRARIES=/path/to/libomptdag.so ./dpotrf_taskdep -n 1024 -b 256`

It will create one `.dot` per parallel region, then use `dot -Tpdf region_X.dot -o dag.pdf`.

### Using OMPT with GCC

There is no support for OMPT in GCC, but you can workaround this because the LLVM runtime is compatible with the libGOMP ABI.

Compile your program (eg: `toy`) with gcc: `gcc -fopenmp toy.c -o toy`.
Depending on how it has been installed, the LLVM runtime may already have a symbolic link between `libgomp.so` and `libomp.so`, you can check which one is used with `ldd`:
```
$ ldd toy
  ...
	libgomp.so.1 => /usr/lib/x86_64-linux-gnu/libgomp.so.1 (0x00007f0e85ccc000)
  ...
```

In this case we need to create the symlink ourselves, in this case in the local directory:
```
$ ln -s ~/install-clang-50/lib/libomp.so libgomp.so.1
$ export LD_LIBRARY_PATH=./:$LD_LIBRARY_PATH
$ ldd toy
  ...
	libgomp.so.1 => ./libgomp.so.1 (0x00007fb1e9f7b000)
  ...
```

Then running the tool should work, even though you did compile with GCC:

```
$ env OMP_TOOL_LIBRARIES=`pwd`/libomptdag.so ./toy
Loading OMPT DAG tool
nested 1
a: 2
et encore a: 2
MAX RSS[KBytes] during execution: 4364
Exiting OMPT DAG tool
```
