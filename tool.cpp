#include "DagEmitter.hpp"

#include <sys/time.h>
#include <sys/resource.h>
#include <omp-tools.h>
#include<iostream>

// See https://git.rwth-aachen.de/OpenMPTools/OMPT-Examples/tree/master/example2

using namespace std;
DagEmitter *emitter;

void
on_ompt_callback_task_create(
    ompt_data_t *parent_task_data,    /* id of parent task            */
    const ompt_frame_t *parent_frame,  /* frame data for parent task   */
    ompt_data_t* new_task_data,      /* id of created task           */
    int type,
    int has_dependences,
    const void *codeptr_ra)               /* pointer to outlined function */
{
  emitter->ActOnTaskCreate(parent_task_data, parent_frame, new_task_data,
      type, has_dependences, codeptr_ra);
}


void
on_ompt_callback_task_schedule(
    ompt_data_t *prior_task_data,
    ompt_task_status_t prior_task_status,
    ompt_data_t *next_task_data)
{
  emitter->ActOnTaskSchedule(prior_task_data, prior_task_status, next_task_data);
}

void
on_ompt_callback_dependences(
    ompt_data_t *task_data,
    const ompt_dependence_t *deps,
    int ndeps)
{
  emitter->ActOnDependencies(task_data, deps, ndeps);
}

void
on_ompt_callback_parallel_begin(
  ompt_data_t *encountering_task_data,
  const ompt_frame_t *encountering_task_frame,
  ompt_data_t *parallel_data,
  unsigned int requested_parallelism,
  int flags,
  const void *codeptr_ra)
{
  emitter->ActOnParallelBegin(encountering_task_data, encountering_task_frame,
      parallel_data, requested_parallelism, flags, codeptr_ra);
}

void on_ompt_callback_parallel_end(
  ompt_data_t *parallel_data,
  ompt_data_t *encountering_task_data,
  int flags,
  const void *codeptr_ra)
{
  emitter->ActOnParallelEnd(parallel_data, encountering_task_data, flags, codeptr_ra);
}

#define register_callback_t(name, type)                       \
do{                                                           \
  type f_##name = &on_##name;                                 \
  if (ompt_set_callback(name, (ompt_callback_t)f_##name) ==   \
      ompt_set_never)                                         \
    cout << "0: Could not register callback '" #name "'\n";   \
}while(0)

#define register_callback(name) register_callback_t(name, name##_t)


void initTool(ompt_function_lookup_t lookup)
{
  emitter = new DagEmitter(lookup);
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  register_callback(ompt_callback_task_create);
  register_callback(ompt_callback_task_schedule);
  register_callback(ompt_callback_dependences);
  register_callback(ompt_callback_parallel_begin);
  register_callback(ompt_callback_parallel_end);
}

void finalizeTool()
{
  struct rusage end;
  getrusage(RUSAGE_SELF, &end);
  cout << "MAX RSS[KBytes] during execution: " << end.ru_maxrss << "\n";
  delete emitter;
}

extern "C" {
  int ompt_initialize(
      ompt_function_lookup_t lookup,
      int initial_device_num,
      ompt_data_t* tool_data)
  {
    initTool(lookup);
    return 1; //success
  }

  void ompt_finalize(ompt_data_t* data)
  {
    finalizeTool();
    cerr << "Exiting OMPT DAG tool\n";
  }

  ompt_start_tool_result_t* ompt_start_tool(
      unsigned int omp_version,
      const char *runtime_version)
  {
    cerr << "Loading OMPT DAG tool\n";
    static ompt_start_tool_result_t ompt_start_tool_result = {&ompt_initialize,&ompt_finalize,0};
    return &ompt_start_tool_result;
  }
}
