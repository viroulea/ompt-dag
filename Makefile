
CXXFLAGS+=-std=c++11 -fPIC -Wall

all: toy libomptdag.so

%.o: %.cpp DagEmitter.hpp
	$(CXX) $(CXXFLAGS) $< -c -o $@


libomptdag.so: tool.o DagEmitter.o
	$(CXX) tool.o DagEmitter.o -shared -fPIC -o $@

toy: toy.c
	clang-4.0 -fopenmp toy.c -o toy

run: toy libomptdag.so
	env OMP_TOOL_LIBRARIES=$(PWD)/libomptdag.so ./toy

run-gtod:
	make clean
	CXXFLAGS="-DENABLE_GTOD" make run

dot:
	OMP_NUM_THREADS=1 make run
	dot -Tpdf region_1.dot -o truc.pdf

clean:
	rm -rf libomptdag.so toy *.o
