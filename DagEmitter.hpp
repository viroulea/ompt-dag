#include <omp-tools.h>
#include <map>
#include <fstream>
#include <mutex>
#include <iostream>
#ifdef ENABLE_GTOD
  #include <sys/time.h>
#endif

class DagEmitter {
  std::ofstream output_;
  std::mutex outputMutex_;
#ifdef ENABLE_GTOD
  std::mutex taskTvLocks;;
  std::map<uint64_t, struct timeval> tasksBegin_;
#endif
  std::map<void *, uint64_t> variableVersions_;
  int currentRegion_ = 1;
  ompt_get_thread_data_t ompt_get_thread_data;
  ompt_get_unique_id_t ompt_get_unique_id;

  void emitData(void *ptr);
  void maybeEmitData(void *ptr);
  void emitInDep(uint64_t taskId, void *data);
  void emitOutDep(uint64_t taskId, void *data);
  void emitTask(uint64_t taskId, ompt_task_flag_t type);
public:
  DagEmitter(ompt_function_lookup_t lookup);
  ~DagEmitter();
  void ActOnTaskCreate(ompt_data_t *parent_task_data,
                       const ompt_frame_t *parent_frame,
                       ompt_data_t* new_task_data,
                       int type, int has_dependences,
                       const void *codeptr_ra);
  void ActOnTaskSchedule(ompt_data_t *prior_task_data,
                         ompt_task_status_t prior_task_status,
                         ompt_data_t *next_task_data);
  void ActOnDependencies(ompt_data_t *task_data,
                         const ompt_dependence_t *deps,
                         int ndeps);
  void ActOnParallelBegin(ompt_data_t *encountering_task_data,
                          const ompt_frame_t *encountering_task_frame,
                          ompt_data_t *parallel_data,
                          unsigned int requested_parallelism,
                          int flags,
                          const void *codeptr_ra);
  void ActOnParallelEnd(ompt_data_t *parallel_data,
                        ompt_data_t *encountering_task_data,
                        int flags,
                        const void *codeptr_ra);
};
