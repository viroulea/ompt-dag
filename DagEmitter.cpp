#include "DagEmitter.hpp"
#include <iostream>

using namespace std;

DagEmitter::DagEmitter(ompt_function_lookup_t lookup)
{
  ompt_get_thread_data = (ompt_get_thread_data_t) lookup("ompt_get_thread_data");
  ompt_get_unique_id = (ompt_get_unique_id_t) lookup("ompt_get_unique_id");
}

DagEmitter::~DagEmitter() {
}


void DagEmitter::emitData(void *ptr)
{
  uint64_t version = variableVersions_[ptr];
  lock_guard<mutex> lock(outputMutex_);
  output_ << "data_" << ptr << "_" << version << " [label=\""
    << ptr << "_" << version << "\", shape=box];\n";
}

void DagEmitter::maybeEmitData(void *ptr)
{
  if (variableVersions_.find(ptr) == variableVersions_.end())
    emitData(ptr);
}

void DagEmitter::emitInDep(uint64_t taskId, void *data)
{
  maybeEmitData(data);
  uint64_t version = variableVersions_[data];
  lock_guard<mutex> lock(outputMutex_);
  output_ << "data_"
    << data << "_" << version
    << " -> "
    << "task_" << (void*)taskId
    << ";\n";
}

void DagEmitter::emitOutDep(uint64_t taskId, void *data)
{
  uint64_t version = ++variableVersions_[data];
  emitData(data);
  {
    lock_guard<mutex> lock(outputMutex_);
    output_ << "task_" << (void*)taskId << " -> "
      << "data_" << data << "_" << version << ";\n";
    output_ << "data_" << data << "_" << version - 1
      << " -> data_" << data << "_" << version
      << " [style=dotted];\n";
  }
}

void DagEmitter::emitTask(uint64_t taskId, ompt_task_flag_t type)
{
  lock_guard<mutex> lock(outputMutex_);
  output_ << "task_" << (void*)taskId << " [label=\""
    << (void*)taskId << "\"";
  if (type & ompt_task_initial)
    output_ << ",shape=doublecircle";
  if (type & ompt_task_implicit)
    output_ << ",style=dotted";
  output_ << "];\n";
}

void DagEmitter::ActOnTaskCreate(ompt_data_t *parent_task_data,
                                 const ompt_frame_t *parent_frame,
                                 ompt_data_t* new_task_data,
                                 int type, int has_dependences,
                                 const void *codeptr_ra)
{
  new_task_data->value = ompt_get_unique_id();
  emitTask(new_task_data->value, (ompt_task_flag_t)type);
}

void DagEmitter::ActOnTaskSchedule(ompt_data_t *prior_task_data,
                                   ompt_task_status_t prior_task_status,
                                   ompt_data_t *next_task_data)
{
// TODO: 'prior_task_data' gives the task finished executed if not null,
// 'next_task_data' gives the task to be executed if not null.
// Should be enough to get some perf counter before/after task
#ifdef ENABLE_GTOD
  if (next_task_data->ptr) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    lock_guard<mutex> lock(taskTvLocks);
    tasksBegin_[next_task_data->value] = tv;
    cerr << "Starting task " << next_task_data->ptr << "\n";
  }

  if (prior_task_data->ptr) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    lock_guard<mutex> lock(taskTvLocks);
    struct timeval before = tasksBegin_[prior_task_data->value];
    unsigned long us = (tv.tv_sec-before.tv_sec)*1000000
      + (tv.tv_usec-before.tv_usec);
    cerr << "Ending task " << prior_task_data->ptr << "; took " << us << " microseconds\n";
  }
#endif
}

void DagEmitter::ActOnDependencies(ompt_data_t *task_data,
                                   const ompt_dependence_t *deps,
                                   int ndeps)
{
  uint64_t taskId = task_data->value;
  for (int i = 0; i < ndeps; i++) {
    switch(deps[i].dependence_type) {
      case ompt_dependence_type_in:
        emitInDep(taskId, deps[i].variable.ptr);
        break;
      case ompt_dependence_type_out:
        emitOutDep(taskId, deps[i].variable.ptr);
        break;
      case ompt_dependence_type_inout:
        emitInDep(taskId, deps[i].variable.ptr);
        emitOutDep(taskId, deps[i].variable.ptr);
        break;
      case ompt_dependence_type_mutexinoutset:
      case ompt_dependence_type_source:
      case ompt_dependence_type_sink:
        break;
    }
  }
}


void DagEmitter::ActOnParallelBegin(ompt_data_t *encountering_task_data,
                                    const ompt_frame_t *encountering_task_frame,
                                    ompt_data_t *parallel_data,
                                    unsigned int requested_parallelism,
                                    int flags,
                                    const void *codeptr_ra)
{
  string filename = "region_" + to_string(currentRegion_) + ".dot";
  output_.open(filename, ios::out);
  output_ << "digraph dag {\n";
#ifdef ENABLE_GTOD
  cerr << "Starting parallel region\n";
#endif
}


void DagEmitter::ActOnParallelEnd(ompt_data_t *parallel_data,
                                  ompt_data_t *encountering_task_data,
                                  int flags,
                                  const void *codeptr_ra)
{
  output_ << "}\n";
#ifdef ENABLE_GTOD
  cerr << "Ending parallel region\n";
#endif
  currentRegion_++;
  output_.close();
  variableVersions_.clear();
}
