#include <stdio.h>
#include <unistd.h>
#include "omp.h"

int main()
{
  int a = 0;
#pragma omp parallel
#pragma omp master
  {
#pragma omp task depend(inout: a)
    {
      a++;
#pragma omp task depend(in: a)
      {
        printf("nested %d\n", a);
      }
      sleep(1);
    }
#pragma omp task depend(inout: a)
    {
      a++;
    }
#pragma omp task depend(in: a)
    {
      printf("a: %d\n", a);

    }
#pragma omp task depend(in: a)
    {
      printf("et encore a: %d\n", a);
    }
  }
}
